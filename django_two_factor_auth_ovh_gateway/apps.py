# Django
from django.apps import AppConfig


class DjangoTwoFactorAuthOvhGateway(AppConfig):
    name = "django_two_factor_auth_ovh_gateway"
